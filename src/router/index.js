import { createRouter, createWebHistory } from 'vue-router';
// import Layout from '@/layouts';
// import RouteView from '@/layouts/route-view';
export const routes = [
  // {
  //   name: 'index',
  //   path: '/',
  //   component: Layout,
  //   redirect: '/home/main',
  //   children: [
  //     {
  //       name: 'home',
  //       path: '/home',
  //       redirect: '/home/main',
  //       component: RouteView,
  //       meta: {
  //         icon: 'icon-shouye-16px',
  //         title: '首页管理',
  //         hideChildrenInMenu: true,
  //       },
  //       children: [
  //         {
  //           name: 'home',
  //           path: '/home/main',
  //           meta: {
  //             title: '首页',
  //           },
  //           component: () => import('@/views/dashboard/workplace'),
  //         },
  //       ],
  //     },
  //   ],
  // },
];
export const staticRoutes = [
  {
    name: 'home',
    path: '/',
    meta: {
      title: '首页',
    },
    component: () => import('@/views/dashboard/workplace'),
  },
  {
    path: '/:pathMatch(.*)',
    component: () => import('@/views/exception/404'),
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes: staticRoutes,
});
export default router;
