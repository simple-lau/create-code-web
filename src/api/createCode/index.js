import request from '@/utils/request';
export function apiExportCode(data) {
  return request.post('create/code', data, { responseType: 'blob' });
}
