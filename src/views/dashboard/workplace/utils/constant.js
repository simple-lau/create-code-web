export const HANDLER = [
  {
    label: '增加',
    value: 'add',
  },
  {
    label: '编辑',
    value: 'edit',
  },
  {
    label: '删除',
    value: 'delete',
  },
  {
    label: '查看',
    value: 'get',
  },
];

export const COMPONENT = [
  {
    label: '输入框',
    value: 'input',
  },
  {
    label: '下拉列表',
    value: 'select',
  },
  {
    label: '日期选择器',
    value: 'datePicker',
  },
];
