export const isObject = key => typeof key === 'object';

export const getLocal = (key, type = 'string') => {
  const item = localStorage.getItem(key);
  return type === 'string' ? item : JSON.parse(item);
};

export const setLocal = (key, value) => {
  const item = isObject(value) ? JSON.stringify(value) : JSON.stringify(value);
  localStorage.setItem(key, item);
};
