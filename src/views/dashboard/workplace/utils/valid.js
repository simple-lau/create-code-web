export const validField = form => {
  return function validator(key) {
    if (form.hasPagination === 1 && !form[key]) {
      Promise.reject('请输入当前分页字段');
    } else {
      Promise.resolve();
    }
  };
};
